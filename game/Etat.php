<?php
class EtatManager
{
	private $_db;

	function __construct($db)
	{
		$this->setdb($db);
	}

	public function setdb($db)
	{
		$this->_db = $db;
	}

	public function add(Etat $etat)
	{
		$q = $this->_db->prepare('INSERT INTO Etat(nom, perception, regime, player)
		VAlUES (:nom, :perception, :regime, :player)');

		$q->bindValue(':nom', $etat->nom());
		$q->bindValue(':perception', $etat->perception());
		$q->bindValue(':regime', $etat->regime());
		$q->bindValue(':player', $etat->player());

		$q->execute();
		return ($this->_db->lastInsertId());
	}

	public function delete(Ville $ville)
	{
		$this->db->exec('DELETE FROM Etat WHERE id = '.$ville->id());
	}

	public function get($id)
	{
		$id = (int) $id;

		$q = $this->_db->query('SELECT id, nom, perception, regime, player
			FROM Etat WHERE id = '.$id);
		$donnee = $q->fetch(PDO::FETCH_ASSOC);

		return new Etat($donnee);
	}

	public function count()
	{
		return $this->_db->query('SELECT COUNT(*) FROM Etat')->fetchColumn();
	}

	public function get_list()
	{
		$Etats = [];

		$q = $this->_db->query('SELECT id, nom, perception, regime, player FROM Etat ORDER BY nom');

		while ($donnee = $q->fetch(PDO::FETCH_ASSOC))
		{
			$Etats[] = new Etat($donnee);
		}

		return ($Etats);
	}

	public function update(Etat $perso)
	{
		$q = $this->_db->prepare('UPDATE Etat SET
			name = :nom, perception = :perception, regime = :regime, player = :player');
		$q->bindValue(':nom', $etat->nom());
		$q->bindValue(':perception', $etat->perception());
		$q->bindValue(':regime', $etat->regime());
		$q->bindValue(':player', $etat->player());

		$q->execute();
	}

	public function exist($info)
	{
		if (is_int($info))
		{
			return (bool) $this->_db->query('SELECT COUNT(*) FROM Etat WHERE id = '.$info)->fetchColumn();
		}
		$q = $this->_db->prepare('SELECT COUNT(*) FROM Etat WHERE nom = :nom');
		$q->execute([':nom' => $info]);
		return (bool) $q->fetchColumn();
	}
}

/**
 *
 */
class Etat
{
	private $_id;
	private $_nom;
	private $_perception;
	private $_player;
	private $_regime;

	function __construct(array $donnees)
	{
		$this->hydrate($donnees);
	}

	public function hydrate(array $donnees)
	{
		foreach ($donnees as $key => $value)
		{
			$method = 'set'.ucfirst($key);
			if (method_exists($this, $method))
			{
				$this->$method($value);
			}
		}
	}

	public function id() {return $this->_id;}
	public function nom() {return $this->_nom;}
	public function perception() {return $this->_perception;}
	public function regime() {return $this->_regime;}
	public function player() {return $this->_player;}

	public function setId($id)
	{
		$this->_id = $id;
	}

	public function setNom($nom)
	{
		if (is_string($nom))
		{
			$this->_nom = $nom;
    	}
	}

	public function setPerception($perception)
	{
		$this->_perception = $perception;
	}

	public function setRegime($regime)
	{
		$this->_regime = $regime;
	}

	public function setPlayer($player)
	{
		$this->_player = $player;
	}
}

/*$Etat = new Etat([
	'nom' => 'test1',
	'perception' => 12,
	'regime' => 'Democracie',
	'player' => 13
]);

$_db = new PDO('mysql:host=mysql;dbname=db_civ;charset=utf8mb4', 'root', 'rootpass');
$manager = new EtatManager($_db);

var_dump($Etat);
$id = $manager->add($Etat);
var_dump($id);*/
?>
