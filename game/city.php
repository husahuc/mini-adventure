<?php

/**
 *
 */
class VilleGenerator
{
	private $_sylabes;
	private $_id_user;

	function __construct($array)
	{
		$this->hydrate($array);
	}

	public function sylabes(){return ($this->_sylabes);}
	public function id_user(){return ($this->_id_user);}

	public function hydrate(array $donnees)
	{
		foreach ($donnees as $key => $value)
		{
			$method = 'set'.ucfirst($key);
			if (method_exists($this, $method))
			{
				$this->$method($value);
			}
		}
	}

	public function Generate()
	{
		$Ville = new Ville ([
			'nom' => $this->Generate_nom(),
			'player' => $this->id_user(),
			'description' => $this->Generate_Description()
		]);
		var_dump($Ville);
		return ($Ville);
	}

	private function Generate_nom()
	{
		$Sylabe = $this->sylabes();
		$nb_sylabe = rand(2, 3);
		$nom = "";
		for ($i=0; $i < $nb_sylabe ; $i++) {
			$nom .= $Sylabe[rand(0, count($Sylabe)-1)];
		}
		return (ucfirst($nom));
	}

	private function Generate_Description()
	{
		return ("une petite ville");
	}

	private function setSylabes($array)
	{
		$this->_sylabes = $array;
	}

	private function setId_user($id)
	{
		$this->_id_user = $id;
	}
}


/**
 *
 */
class VilleManager
{
	private $_db; // PDO

	public function __construct($db)
	{
		$this->setdb($db);
	}

	public function setdb($db)
	{
		$this->_db = $db;
	}

	public function add(Ville $ville)
	{
		$q = $this->_db->prepare('INSERT INTO Ville(nom, position, description, if_player)
		VAlUES (:nom, :position, :description, :if_player)');

		$q->bindValue(':nom', $ville->nom());
		$q->bindValue(':position', $ville->position());
		$q->bindValue(':description', $ville->description());
		$q->bindValue(':if_player', $ville->player(), PDO::PARAM_INT);

		$q->execute();
	}

	public function delete(Ville $ville)
	{
		$this->db->exec('DELETE FROM Ville WHERE id = '.$ville->id());
	}

	public function get($id)
	{
		$id = (int) $id;

		$q = $this->_db->query('SELECT id, nom, position, description, if_player
			FROM Ville WHERE id = '.$id);
		$donnee = $q->fetch(PDO::FETCH_ASSOC);

		return new Ville($donnee);
	}

	public function count()
	{
		return $this->_db->query('SELECT COUNT(*) FROM Ville')->fetchColumn();
	}

	public function get_list()
	{
		$villes = [];

		$q = $this->_db->query('SELECT id, nom, position, description, if_player FROM Ville ORDER BY nom');

		while ($donnee = $q->fetch(PDO::FETCH_ASSOC))
		{
			$villes[] = new Ville($donnee);
		}

		return ($villes);
	}

	public function update(Ville $ville)
	{
		$q = $this->_db->prepare('UPDATE Ville SET nom = :nom, position = :position, description = :description, if_player = :if_player');

		$q->bindValue(':nom', $ville->nom());
		$q->bindValue(':position', $ville->position());
		$q->bindValue(':description', $ville->desc());
		$q->bindValue(':if_player', $ville->player(), PDO::PARAM_INT);

		$q->execute();
	}

	public function exist($info)
	{
		if (is_int($info))
		{
			return (bool) $this->_db->query('SELECT COUNT(*) FROM Ville WHERE id = '.$info)->fetchColumn();
		}
		$q = $this->_db->prepare('SELECT COUNT(*) FROM Ville WHERE nom = :nom');
		$q->execute([':nom' => $info]);
		return (bool) $q->fetchColumn();
	}
}


/**
 *
 */
class Ville
{
	private $_id;
	private $_nom;
	private $_description;
	private $_position;
	private $_player;

	public function __construct(array $donnees)
	{
		$this->hydrate($donnees);
	}

	public function hydrate(array $donnees)
	{
		foreach ($donnees as $key => $value)
		{
			$method = 'set'.ucfirst($key);
			if (method_exists($this, $method))
			{
				$this->$method($value);
			}
		}
	}

	public function id() {return $this->_id;}
	public function nom() {return $this->_nom;}
	public function description() {return $this->_description;}
	public function player() {return $this->_player;}
	public function position() {return $this->_position;}

	public function setId($id)
	{
		$this->_id = $id;
	}

	public function setNom($nom)
	{
		if (is_string($nom))
		{
			$this->_nom = $nom;
		}
	}

	public function setDescription($description)
	{
		if (is_string($description))
			$this->_description = $description;
	}

	public function setPlayer($player)
	{
		$this->_player = $player;
	}

	public function setPosition($position)
	{
		$this->_position = $position; // future implementation de la position en points
	}

	public function construire_batiment()
	{

	}

	public function evenement()
	{

	}

	public function tour()
	{
		echo "my name is J'".$this->nom();
	}
}
?>
