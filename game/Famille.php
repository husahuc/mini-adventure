<?php
/**
 *
 */
class FamilleGenerator
{
	private $_sylabes;
	private $_id_user;

	function __construct($array)
	{
		$this->hydrate($array);
	}

	public function sylabes(){return ($this->_sylabes);}
	public function id_user(){return ($this->_id_user);}

	public function hydrate(array $donnees)
	{
		foreach ($donnees as $key => $value)
		{
			$method = 'set'.ucfirst($key);
			if (method_exists($this, $method))
			{
				$this->$method($value);
			}
		}
	}

	public function Generate()
	{
		$Famille = new Famille ([
			'nom' => $this->Generate_nom(),
			'city' => $this->City(),
			'members' => $this->GenerateMembers(),
			'richesse' => $this->GenerateRichesse(),
			'influence' => $this->GenerateInfluence(),
			'solidarite' => $this->GenerateSolidarite(),
			'opinion' => $this->GenerateOpinion(),
			'production' => $this->GenerateProduction()
		]);
		return ($Famille);
	}

	private function Generate_nom()
	{
		$Sylabe = $this->sylabes();
		$nb_sylabe = rand(2, 3);
		$nom = "";
		for ($i=0; $i < $nb_sylabe ; $i++) {
			$nom .= $Sylabe[rand(0, count($Sylabe)-1)];
		}
		return (ucfirst($nom));
	}

	public function GenerateMembers(){return (rand(13, 20));}
	public function GenerateRichesse(){return (rand(50, 17));}
	public function GenerateMembers(){return (rand(13, 20));}
	public function GenerateMembers(){return (rand(13, 20));}
	public function GenerateMembers(){return (rand(13, 20));}
	public function GenerateMembers(){return (rand(13, 20));}

	private function setSylabes($array){$this->_sylabes = $array;}

	private function setId_user($id){$this->_id_user = $id;}
}


/**
 *
 */
class FamilleManager
{
	private $_db; // PDO

	public function __construct($db)
	{
		$this->setdb($db);
	}

	public function setdb($db)
	{
		$this->_db = $db;
	}

	public function add(Famille $Famille)
	{
		$q = $this->_db->prepare('INSERT INTO Famille(nom, position, description, if_player)
		VAlUES (:nom, :city, :members, :richesse, :influence, :solidarite, :opinion, :production)');

		$q->bindValue(':nom', $Famille->nom());
		$q->bindValue(':city', $Famille->city());
		$q->bindValue(':members', $Famille->members());
		$q->bindValue(':richesse', $Famille->richesse());
		$q->bindValue(':influence', $Famille->influence());
		$q->bindValue(':solidarite', $Famille->solidarite());
		$q->bindValue(':opinion', $Famille->opinion());
		$q->bindValue(':production', $Famille->production());

		$q->execute();
	}

	public function delete(Famille $Famille)
	{
		$this->db->exec('DELETE FROM Famille WHERE id = '.$Famille->id());
	}

	public function get($id)
	{
		$id = (int) $id;

		$q = $this->_db->query('SELECT id, nom, city, members, richesse, influence, solidarite, opinion, production
			FROM Famille WHERE id = '.$id);
		$donnee = $q->fetch(PDO::FETCH_ASSOC);

		return new Famille($donnee);
	}

	public function count()
	{
		return $this->_db->query('SELECT COUNT(*) FROM Famille')->fetchColumn();
	}

	public function get_list()
	{
		$Familles = [];

		$q = $this->_db->query('SELECT id, nom, city, members, richesse, influence, solidarite, opinion, production FROM Famille ORDER BY nom');

		while ($donnee = $q->fetch(PDO::FETCH_ASSOC))
		{
			$Familles[] = new Famille($donnee);
		}

		return ($Familles);
	}

	public function update(Famille $Famille)
	{
		$q = $this->_db->prepare('UPDATE Famille SET nom = :nom, city =:city, members =:members , richesse =:richesse , influence =:influence
			,solidarite =:solidarite ,opinion =:opinion ,production =:production');

		$q->bindValue(':nom', $Famille->nom());
		$q->bindValue(':city', $Famille->city());
		$q->bindValue(':members', $Famille->members());
		$q->bindValue(':richesse', $Famille->richesse());
		$q->bindValue(':influence', $Famille->influence());
		$q->bindValue(':solidarite', $Famille->solidarite());
		$q->bindValue(':opinion', $Famille->opinion());
		$q->bindValue(':production', $Famille->production());

		$q->execute();
	}

	public function exist($info)
	{
		if (is_int($info))
		{
			return (bool) $this->_db->query('SELECT COUNT(*) FROM Famille WHERE id = '.$info)->fetchColumn();
		}
		$q = $this->_db->prepare('SELECT COUNT(*) FROM Famille WHERE nom = :nom');
		$q->execute([':nom' => $info]);
		return (bool) $q->fetchColumn();
	}
}

/**
 *
 */
class Famille
{
	private $_id;
	private $_nom;
	private $_city;
	private $_members;
	private $_habitat;
	private $_richesse;
	private $_influence;
	private $_solidarite;
	private $_opinion;
	private $_production;

	function __construct(array $donnees)
	{
		$this->hydrate($donnes);
	}

	public function hydrate(array $donnees)
	{
		foreach ($donnees as $key => $value)
		{
			$method = 'set'.ucfirst($key);
			if (method_exists($this, $method))
			{
				$this->$method($value);
			}
		}
	}

	public function id() {return $this->_id;}
	public function nom() {return $this->_nom;}
	public function city() {return $this->_city;}
	public function members() {return $this->_members;}
	public function habitat() {return $this->_habitat;}
	public function richesse() {return $this->_richesse;}
	public function influence() {return $this->_influence;}
	public function solidarite() {return $this->_solidarite;}
	public function opinion() {return $this->_opinion;}
	public function production() {return $this->_production;}

	public function setId($id) {$this->_id = $id;}
	public function setId($nom) {$this->_nom = $nom;}
	public function setId($city) {$this->_city = $city;}
	public function setId($members) {$this->_members = $members;}
	public function setId($habitat) {$this->_habitat = $habitat;}
	public function setId($richesse) {$this->_richesse = $richesse;}
	public function setId($influence) {$this->_influence = $influence;}
	public function setId($solidarite) {$this->_solidarite = $solidarite;}
	public function setId($opinion) {$this->_opinion = $opinion;}
	public function setId($production) {$this->_production = $production;}

}

?>
