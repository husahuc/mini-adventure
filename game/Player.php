<?php
include ('game/city.php');

class PlayerManager
{
	private $_db;

	function __construct($db)
	{
		$this->setdb($db);
	}

	public function setdb($db)
	{
		$this->_db = $db;
	}

	public function add(Player $player)
	{
		$q = $this->_db->prepare('INSERT INTO Player(personage, parti, etat, tour, session_pass)
		VAlUES (:personage, :parti, :etat, :tour, :session_pass)');

		$q->bindValue(':personage', $player->personage());
		$q->bindValue(':parti', $player->parti());
		$q->bindValue(':etat', $player->etat());
		$q->bindValue(':tour', $player->tour());
		$q->bindValue(':session_pass', $player->session_pass());

		$q->execute();
		return ($this->_db->lastInsertId());
	}

	public function delete(Ville $ville)
	{
		$this->db->exec('DELETE FROM Player WHERE id = '.$ville->id());
	}

	public function get($id)
	{
		$id = (int) $id;

		$q = $this->_db->query('SELECT id, personage, parti, etat, tour, session_pass
			FROM Player WHERE id = '.$id);
		$donnee = $q->fetch(PDO::FETCH_ASSOC);

		return new Player($donnee);
	}

	public function get_city($id)
	{
		$cities = [];

		$q = $this->_db->query('SELECT Ville.nom, Ville.description FROM `Player` INNER JOIN Ville ON Ville.if_player = Player.id WHERE Player.id = '.$id);
		while ($donnee = $q->fetch(PDO::FETCH_ASSOC))
		{
			$cities[] = new Ville($donnee);
		}

		return ($cities);
	}

	public function count()
	{
		return $this->_db->query('SELECT COUNT(*) FROM Player')->fetchColumn();
	}

	public function get_list()
	{
		$Players = [];

		$q = $this->_db->query('SELECT id, personage, parti, etat, tour FROM Player ORDER BY id');

		while ($donnee = $q->fetch(PDO::FETCH_ASSOC))
		{
			$Players[] = new Player($donnee);
		}

		return ($Players);
	}

	public function update(Player $perso)
	{
		$q = $this->_db->prepare('UPDATE Player SET
			personage = :personage, parti = :parti, etat = :etat, tour = :tour, session_pass = :session_pass');

		$q->bindValue(':personage', $ville->personage());
		$q->bindValue(':parti', $ville->parti());
		$q->bindValue(':etat', $ville->etat());
		$q->bindValue(':tour', $ville->tour());
		$q->bindValue(':session_pass', $ville->session_pass());

		$q->execute();
	}

	public function exist($info)
	{
		return (bool) $this->_db->query('SELECT COUNT(*) FROM Player WHERE id = '.$info)->fetchColumn();
	}
}

/**
 *
 */
class Player
{
	private $_id;
	private $_personage;
	private $_parti;
	private $_etat;
	private $_session_pass;
	private $_tour;

	function __construct(array $donnees)
	{
		$this->hydrate($donnees);
	}

	public function hydrate(array $donnees)
	{
		foreach ($donnees as $key => $value)
		{
			$method = 'set'.ucfirst($key);
			if (method_exists($this, $method))
			{
				$this->$method($value);
			}
		}
	}

	public function id() {return $this->_id;}
	public function personage() {return $this->_personage;}
	public function parti() {return $this->_parti;}
	public function etat() {return $this->_etat;}
	public function tour() {return $this->_tour;}
	public function session_pass() {return $this->_session_pass;}

	public function setId($id)
	{
		$this->_id = $id;
	}

	public function setPersonage($id_perso)
	{
		$id_perso = (int) $id_perso;
		if ($id_perso >= 0)
			$this->_personage = $id_perso;
	}

	public function setParti($id_parti)
	{
		$id_parti = (int) $id_parti;
		if ($id_parti >= 0)
			$this->_parti = $id_parti;
	}

	public function setEtat($id_etat)
	{
		$id_etat = (int) $id_etat;
		if ($id_etat >= 0)
			$this->_etat = $id_etat;
	}

	public function setTour($tour)
	{
		$tour = (int) $tour;
		if ($tour >= 0)
			$this->_tour = $tour;
	}

	public function setSession_pass($session_pass)
	{
		if (is_string($session_pass))
			$this->_session_pass = $session_pass;
	}

	public function tour_player()
	{
		// recuperer toutes les ville puis faire un tour;
		$manager = new PlayerManager(new PDO('mysql:host=mysql;dbname=db_civ;charset=utf8mb4', 'root', 'rootpass'));
		$Villes = $manager->get_city($this->id());
		foreach ($Villes as $Ville)
		{
			$Ville->tour();
		}
		$this->setTour($this->tour() + 1);
	}
}
?>
