<?php
class PartiManager
{
	private $_db;

	function __construct($db)
	{
		$this->setdb($db);
	}

	public function setdb($db)
	{
		$this->_db = $db;
	}

	public function add(Parti $ville)
	{
		$q = $this->_db->prepare('INSERT INTO Parti(nom, organisation)
		VAlUES (:nom, :organisation)');

		$q->bindValue(':nom', $ville->nom());
		$q->bindValue(':organisation', $ville->organisation());

		$q->execute();
		return ($this->_db->lastInsertId());
	}

	public function delete(Ville $ville)
	{
		$this->db->exec('DELETE FROM Parti WHERE id = '.$ville->id());
	}

	public function get($id)
	{
		$id = (int) $id;

		$q = $this->_db->query('SELECT id, nom, organisation
			FROM Parti WHERE id = '.$id);
		$donnee = $q->fetch(PDO::FETCH_ASSOC);

		return new Parti($donnee);
	}

	public function count()
	{
		return $this->_db->query('SELECT COUNT(*) FROM Parti')->fetchColumn();
	}

	public function get_list()
	{
		$Partis = [];

		$q = $this->_db->query('SELECT id, nom, organisation FROM Parti ORDER BY nom');

		while ($donnee = $q->fetch(PDO::FETCH_ASSOC))
		{
			$Partis[] = new Parti($donnee);
		}

		return ($Partis);
	}

	public function update(Parti $perso)
	{
		$q = $this->_db->prepare('UPDATE Parti SET
			name = :nom, Organisation = :organisation');

		$q->bindValue(':nom', $ville->nom());
		$q->bindValue(':organisation', $ville->nom());

		$q->execute();
	}

	public function exist($info)
	{
		if (is_int($info))
		{
			return (bool) $this->_db->query('SELECT COUNT(*) FROM Parti WHERE id = '.$info)->fetchColumn();
		}
		$q = $this->_db->prepare('SELECT COUNT(*) FROM Parti WHERE nom = :nom');
		$q->execute([':nom' => $info]);
		return (bool) $q->fetchColumn();
	}
}

/**
 *
 */
class Parti
{
	private $_id;
	private $_nom;
	private $_organisation;

	function __construct(array $donnees)
	{
		$this->hydrate($donnees);
	}

	public function hydrate(array $donnees)
	{
		foreach ($donnees as $key => $value)
		{
			$method = 'set'.ucfirst($key);
			if (method_exists($this, $method))
			{
				$this->$method($value);
			}
		}
	}

	public function id() {return $this->_id;}
	public function nom() {return $this->_nom;}
	public function organisation() {return $this->_organisation;}

	public function setId($id)
	{
		$this->_id = $id;
	}

	public function setNom($nom)
	{
		if (is_string($nom))
		{
			$this->_nom = $nom;
		}
	}

	public function setOrganisation($orga)
	{
		if (is_string($orga))
		{
			$this->_organisation = $orga;
		}
	}
}

/*$Parti = new Parti([
	'nom' => 'test1',
	'description' => 'un test simple',
	'genre' => 'masculin',
	'origines' => 'autre',
	'perception' => 12,
	'charisme' => 13,
	'id_city' => 12,
	'if_player' => 12
]);

$_db = new PDO('mysql:host=mysql;dbname=db_civ;charset=utf8mb4', 'root', 'rootpass');
$manager = new PartiManager($_db);

var_dump($Parti);
$manager->add($Parti);*/
?>
