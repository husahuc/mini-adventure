<?php
/**
 *
 */
class PersonageManager
{
	private $_db;

	function __construct($db)
	{
		$this->setdb($db);
	}

	public function setdb($db)
	{
		$this->_db = $db;
	}

	public function add(Personage $ville)
	{
		$q = $this->_db->prepare('INSERT INTO Personage(nom, description, genre, origines, perception, charisme, id_city, if_player)
		VAlUES (:nom, :description, :genre, :origines, :perception, :charisme, :id_city, :if_player)');

		$q->bindValue(':nom', $ville->nom());
		$q->bindValue(':description', $ville->description());
		$q->bindValue(':genre', $ville->genre());
		$q->bindValue(':origines', $ville->origines());
		$q->bindValue(':perception', $ville->perception());
		$q->bindValue(':charisme', $ville->charisme());
		$q->bindValue(':id_city', $ville->id_city());
		$q->bindValue(':if_player', $ville->if_player());

		$q->execute();
		return ($this->_db->lastInsertId());
	}

	public function delete(Ville $ville)
	{
		$this->db->exec('DELETE FROM Personage WHERE id = '.$ville->id());
	}

	public function get($id)
	{
		$id = (int) $id;

		$q = $this->_db->query('SELECT id, nom, description, genre, origines, perception, charisme, id_city, if_player
			FROM Personage WHERE id = '.$id);
		$donnee = $q->fetch(PDO::FETCH_ASSOC);

		return new Personage($donnee);
	}

	public function count()
	{
		return $this->_db->query('SELECT COUNT(*) FROM Personage')->fetchColumn();
	}

	public function get_list()
	{
		$personages = [];

		$q = $this->_db->query('SELECT id, nom, description, genre, origines, perception, charisme, id_city, if_player FROM Personage ORDER BY nom');

		while ($donnee = $q->fetch(PDO::FETCH_ASSOC))
		{
			$personages[] = new Personage($donnee);
		}

		return ($personages);
	}

	public function update(Personage $perso)
	{
		$q = $this->_db->prepare('UPDATE Personage SET
			name = :nom, description = :description, genre = :genre, origines = :origines, perception = :perception, charisme = :charisme, id_city = :id_city, if_player = :if_player');

		$q->bindValue(':nom', $ville->nom());
		$q->bindValue(':description', $ville->description());
		$q->bindValue(':genre', $ville->genre());
		$q->bindValue(':origines', $ville->origines());
		$q->bindValue(':perception', $ville->perception());
		$q->bindValue(':charisme', $ville->charisme());
		$q->bindValue(':id_city', $ville->id_city());
		$q->bindValue(':if_player', $ville->if_player());

		$q->execute();
	}

	public function exist($info)
	{
		if (is_int($info))
		{
			return (bool) $this->_db->query('SELECT COUNT(*) FROM Personage WHERE id = '.$info)->fetchColumn();
		}
		$q = $this->_db->prepare('SELECT COUNT(*) FROM Personage WHERE nom = :nom');
		$q->execute([':nom' => $info]);
		return (bool) $q->fetchColumn();
	}
}

/**
 *
 */
class Personage
{
	private $_id;
	private $_nom;
	private $_description;
	private $_genre;
	private $_origines;
	private $_perception;
	private $_charisme;
	private $_id_city;
	private $_if_player;

	function __construct(array $donnees)
	{
		$this->hydrate($donnees);
	}

	public function hydrate(array $donnees)
	{
		foreach ($donnees as $key => $value)
		{
			$method = 'set'.ucfirst($key);
			if (method_exists($this, $method))
			{
				$this->$method($value);
			}
		}
	}

	public function id() {return $this->_id;}
	public function nom() {return $this->_nom;}
	public function description() {return $this->_description;}
	public function genre() {return $this->_genre;}
	public function origines() {return $this->_origines;}
	public function perception() {return $this->_perception;}
	public function charisme() {return $this->_charisme;}
	public function id_city() {return $this->_id_city;}
	public function if_player() {return $this->_if_player;}

	public function setId($id)
	{
		$this->_id = $id;
	}

	public function setNom($nom)
	{
		if (is_string($nom))
		{
			$this->_nom = $nom;
    	}
	}

	public function setDescription($description)
	{
		if (is_string($description))
			$this->_description = $description;
	}

	public function setGenre($genre)
	{
		if (is_string($genre))
			$this->_genre = $genre;
	}

	public function setOrigines($origine)
	{
		if (is_string($origine))
			$this->_origines = $origine;
	}

	public function setPerception($perception)
	{
		$this->_perception = $perception;
	}

	public function setCharisme($charisme)
	{
		$this->_charisme = $charisme;
	}

	public function setId_city($id)
	{
		$this->_id_city = $id;
	}

	public function setIf_player($player)
	{
		$this->_if_player = $player;
	}
}

/*$personage = new Personage([
	'nom' => 'test1',
	'description' => 'un test simple',
	'genre' => 'masculin',
	'origines' => 'autre',
	'perception' => 12,
	'charisme' => 13,
	'id_city' => 12,
	'if_player' => 12
]);

$_db = new PDO('mysql:host=mysql;dbname=db_civ;charset=utf8mb4', 'root', 'rootpass');
$manager = new PersonageManager($_db);

var_dump($personage);
$manager->add($personage);*/
?>
