<?php
include 'modele/modele.php';
include 'game/Etat.php';
include 'game/personage.php';
include 'game/Parti.php';
include 'game/Player.php';

function corr_input($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

function Generate_pass()
{
	$adjectif = ["jeune"];
	$nom = ["pays"];
	$verbe = ["abaisse"];
	$pass = $adjectif[rand(0, count($adjectif)-1)];
	$pass .= $nom[rand(0, count($nom)-1)];
	$pass .= $verbe[rand(0, count($verbe)-1)];
	return $pass;

}

function put_form_inscription()
{
	$_db = new PDO('mysql:host=mysql;dbname=db_civ;charset=utf8mb4', 'root', 'rootpass');

	$Etat = new Etat([
		'nom' => corr_input($_POST["nom_etat"]),
		'perception' => rand(40, 60),
		'regime' => corr_input($_POST["regime"]),
		'player' => -1
	]);

	$personage = new Personage([
		'nom' => corr_input($_POST["nom_perso"]),
		'description' => "ok",
		'genre' => corr_input($_POST["genre"]),
		'origines' => corr_input($_POST['origine']),
		'perception' => rand(40, 60),
		'charisme' => rand(30, 55),
		'id_city' => -1,
		'if_player' => -1
	]);

	$parti = new Parti([
		'nom' => corr_input($_POST["nom_parti"]),
		'organisation' => corr_input($_POST["sucession"])
	]);

	$manager_perso = new PersonageManager($_db);
	$manager_etat = new EtatManager($_db);
	$manager_parti = new PartiManager($_db);
	$id_etat = $manager_etat->add($Etat);
	$id_perso = $manager_perso->add($personage);
	$id_parti = $manager_parti->add($parti);

	$Player = new Player([
		'personage' => $id_perso,
		'parti' => $id_parti,
		'etat' => $id_etat,
		'session_pass' => Generate_pass(),
		'tour' => 0,
	]);

	$manager_player = new PlayerManager($_db);
	$id_player = $manager_player->add($Player);
	session_start();
	$_SESSION['is_log'] = 'ok';
	$_SESSION['id_user'] = $id_player;
	var_dump($Player);
	//header('location: start');
}

function verif_form_inscription()
{
	if (empty($_POST["submit"]) || $_POST["submit"] != "S'inscrire")
		$error = "not ok";
	else if (empty($_POST["nom_perso"]) || empty($_POST["genre"]) || empty($_POST["origine"]))
		$error_perso = "not only";
	else if (empty($_POST["nom_parti"]) || empty($_POST["sucession"]))
		$error_parti = "not ok";
	else if (empty($_POST["nom_etat"]) || empty($_POST["regime"]))
		$error_etat = "not oo";
	else
		put_form_inscription();
}

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
	verif_form_inscription();
}
?>
