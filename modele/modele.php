<?php
function connect_base_civ()
{
	$user = "root";
	$password = "rootpass";
	$database = 'db_civ';

	$conn = new mysqli("mysql", $user, $password, $database);

	if ($conn->connect_error)
	{
	    die("Connection failed: " . $conn->connect_error);
	}
	return ($conn);
}

function executer_sql($sql, $param=NULL)
{
	$conn = connect_base_civ();
	if ($param == NULL)
		$result = $conn->query($sql);
	else
	{
		$result = $conn->prepare($sql);
		$result->execute($param);
	}
	return ($result);
}
?>
