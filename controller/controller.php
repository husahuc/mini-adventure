<?php

include 'vendor/autoload.php';

function render_twig($name)
{
	$loader = new Twig_Loader_Filesystem('Vue_Twig');
	$twig = new Twig_Environment($loader, [
		'cache' => false,
	]);
	return ($twig->render($name));
}

function inscription()
{
	include('modele/inscription.php');
	//require('vue/inscription.php');
	echo render_twig('inscription.twig');
}

function start()
{
	include('modele/start.php');
	//require('vue/start.php');
	echo render_twig('start.twig');
}

function play()
{
	include('modele/play.php');
	echo render_twig('play.twig');
}

function continuer()
{
	include('modele/continuer.php');
	//require('vue/continuer.php');
	echo render_twig('continue.twig');
}

function menu()
{
	//require('Vue_Twig/Gabarit.twig');

	//echo $twig->render('Gabarit.twig');
	echo render_twig('menu.twig');
}
?>
