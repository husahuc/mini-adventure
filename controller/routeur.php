<?php
include('controller.php');

include 'vendor/autoload.php';

// toujours besoin actuelement de index.php
function routeur()
{
	$url = explode('/', $_SERVER['REQUEST_URI']);
	if (isset($url[1]) && !empty($url[1]))
	{
		if ($url[1] == "inscription")
			inscription();
		elseif ($url[1] == "start")
			start();
		elseif ($url[1] == "play")
			play();
		elseif ($url[1] == "continuer")
			continuer();
		else
			echo "ok";
	}
	else
	{
		menu();
	}
}
routeur();
?>
